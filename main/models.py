from django.db import models
# Create your models here.

class Matkul(models.Model):
    Nama_Matkul = models.CharField("Nama_Matkul", max_length = 50)
    Nama_Dosen = models.CharField("Nama_Dosen", max_length = 50)
    Jumlah_SKS = models.CharField("Jumlah_SKS", max_length = 50)
    Deskripsi_Matkul = models.CharField("Deskripsi_Matkul", max_length = 200)
    Tahun_Ajaran = models.CharField("Tahun_Ajaran", max_length = 50)
    Ruang_Kelas = models.CharField("Ruang_Kelas", max_length = 50)

class Post(models.Model):
    author = models.ForeignKey(Matkul, on_delete = models.CASCADE)
