from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('ubahdata/', views.ubahdata, name='ubahdata'),
    path('ubahdata/savedata', views.savedata, name='ubahdata/savedata'),
    path('viewdata/', views.viewdata, name='viewdata'),
]
