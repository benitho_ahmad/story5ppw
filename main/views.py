from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import InputForm
from .models import Matkul, Post

def home(request):
    return render(request, 'main/home.html')

def ubahdata(request):
    context ={'form' : InputForm } 
    return render(request, "main/ubahdata.html", context)

def savedata(request):
    form = InputForm(request.GET or None)
    if (form.is_valid and request.method == "GET"):
        form.save()
        return HttpResponseRedirect('/viewdata')

def viewdata(request):
    obj = Matkul.objects.all()
    return render(request, "main/viewdata.html", {'matkul': obj})