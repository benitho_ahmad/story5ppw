from django import forms
from .models import Matkul

class InputForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ["Nama_Matkul", "Nama_Dosen", "Jumlah_SKS", "Deskripsi_Matkul", "Tahun_Ajaran", "Ruang_Kelas"]

    Nama_Matkul = forms.CharField(max_length = 50)
    Nama_Dosen = forms.CharField(max_length = 50)
    Jumlah_SKS = forms.CharField(max_length = 50)
    Deskripsi_Matkul = forms.CharField(widget = forms.Textarea, max_length = 200)
    Tahun_Ajaran = forms.CharField(max_length = 50)
    Ruang_Kelas = forms.CharField(max_length = 50)
